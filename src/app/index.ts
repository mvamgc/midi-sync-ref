import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {MidiMessageRouterService} from './services/MidiMessageRouterService';
import {Octave} from './keyboard/octave.component';
import {MainComponent} from './main';
import {Keyboard} from './keyboard/keyboard.component';
import {SoundFontPlayerService} from './services/SoundFontPlayerService';

@NgModule({
  imports: [
    BrowserModule,
  ],
  declarations: [
    MainComponent, Keyboard, Octave
  ],
  providers: [MidiMessageRouterService, SoundFontPlayerService],
  bootstrap: [MainComponent]
})
export class AppModule {}
