import {Component, Input} from '@angular/core';
import {MidiMessageRouterService} from '../services/MidiMessageRouterService';

@Component({
  selector: 'octave',
  template: require('./octave.html')
})
export class Octave {
  pressedKeys = new Set();
  @Input() octave: string;

  constructor(private messageRouterService: MidiMessageRouterService) {
  }

  keyPress(note: string) {
    console.log(`key press: ${note} for octave ${this.octave}`);
    this.pressedKeys.add(note);
    setTimeout(() => this.pressedKeys.delete(note), 5000);
  }
  keyDepress(note: string) {
    console.log(`key depress: ${note}`);
    this.pressedKeys.delete(note);
  }
  keyClick(note: string) {
    console.log(`key click: ${note} for octave ${this.octave}`);
    this.messageRouterService.dispatchMidiMessage({
      note: {
        name: note,
        octave: parseInt(this.octave, 10)
      }
    });
  }
  isPressed(note: string): boolean {
    return this.pressedKeys.has(note);
  }
}
