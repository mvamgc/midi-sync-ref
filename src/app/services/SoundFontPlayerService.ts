import * as Soundfont from 'soundfont-player';

import {Injectable} from '@angular/core';
import {IMidiMessageConsumer} from './IMidiMessageConsumer';
import {IMidiMessage} from '../MidiMessage';

@Injectable()
export class SoundFontPlayerService implements IMidiMessageConsumer {
  instrument = 'acoustic_grand_piano';
  piano: any;
  initializing = false;

  sendMidiMessage(midiMessage: IMidiMessage) {
    if (this.piano) {
      this.piano.start(midiMessage.note.name + (midiMessage.note.octave + 2));
    }
  }

  init() {
    if (!this.initializing && !this.piano) {
      Soundfont.instrument(new AudioContext(), this.instrument).then(function (piano: any) {
        this.piano = piano;
        this.initializing = false;
      }.bind(this));
    }
  }

}
